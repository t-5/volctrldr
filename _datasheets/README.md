Datasheets folder
=================

Attention, these are not copyright by me but the individual copyright
holders mentioned inside the PDF files.

If you are the copyright holder and don't want your datasheet(s) published
here, please contact me at t-5@t-5.eu and I will immediately remove them.
Otherwise I assume it is OK to include them here as they are public 
material anyway.
