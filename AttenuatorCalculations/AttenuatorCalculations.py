#!/usr/bin/python3
import math

RIN_RANGE = 800, 4000
DB_STEPS = -1, -2, -4, -8, -16, -32
N = 96 # E-Range for resistors
TOLERANCE = 0.001
OPTIMIZE_LAST_TWO_STEPS = True
DEBUG = False

# functions ######################################################################################

def dbToGainFactor(dbValue):
    return pow(10, dbValue / 20.0)

def gainFactorToDb(gain):
    if gain == 0.0:
        return -1000000
    return 20 * math.log10(gain)

def findNearestER(R_destination):
    global E_RESISTOR_SERIES
    found_R, deviation = 100000, 100000000000
    for E_R in E_RESISTOR_SERIES:
        tmp_deviation = abs(R_destination - E_R)
        if tmp_deviation < deviation:
            deviation = tmp_deviation
            found_R = E_R
    return found_R

def parallelResistors(R_1, R_2):
    return (R_1 * R_2) / (R_1 + R_2)

def solveResistorsFor(R_in, ratio):
    # 0: PAR = 1 / (1/Rpar + 1/Rin)
    # 1: Rin = PAR + Rser
    # 1a: Rser = Rin - PAR
    # 1b: Rser = Rin - (1 / (1/Rpar + 1/Rin))
    # 2: ratio = PAR / Rin
    # 2a: ratio = (1 / (1/Rpar + 1/Rin)) / Rin
    # 2c: (1/Rpar + 1/Rin) = 1 / Rin / ratio
    # 2d: 1 / Rpar = (1 / Rin / ratio) - (1 / Rin)
    R_par = 1 / ((1 / R_in / ratio) - (1 / R_in))
    R_ser = R_in - (1 / (1 / R_par + 1 / R_in))
    return R_par, R_ser


# E-series Rs ####################################################################################
E_RESISTOR_SERIES = []
for decade in (10, 100, 1000, 10000, 100000):
    for m in range(0, N):
        factor = round((10 ** m) ** (1 / N), 2)
        E_RESISTOR_SERIES.append(factor * decade)

# cycle through all Rin combinations and compare them to the E series values & calc. deviations ##

result = {}  # holds a mapping of deviation -> (Rin, RFBs, ERFBs)

print("running simulation for Rin = ", end="")
for R_in in filter(lambda x: RIN_RANGE[0] < x < RIN_RANGE[1], E_RESISTOR_SERIES):
    print("%d, " % R_in, end="")
    attenuatorValues = []
    E_attenuatorValues = []
    for idx, db in enumerate(DB_STEPS):
        ratio = dbToGainFactor(db)
        R_par, R_ser = solveResistorsFor(R_in, ratio)
        attenuatorValues.append((R_par, R_ser))
        E_attenuatorValues.append([findNearestER(R_par), findNearestER(R_ser)])
    if DEBUG:
        print(attenuatorValues)
        print(E_attenuatorValues)

    # calculate deviations
    dev = 0
    for step in range(0, 64):
        if DEBUG:
            print("-%sdB:" % step)
        #destRatio = dbToGainFactor(-step)
        ratio = 1
        Eratio = 1
        for idx in range(0, len(DB_STEPS)):
            if step & (0b1 << idx):
                # print("  b:", bin(0b1 << idx))
                R_par, R_ser = attenuatorValues[idx]
                E_R_par, E_R_ser = E_attenuatorValues[idx]
                par = parallelResistors(R_par, R_in)
                E_par = parallelResistors(E_R_par, R_in)
                tmp_ratio = par / (R_ser + par)
                E_tmp_ratio = E_par / (E_R_ser + E_par)
                tmp_att = gainFactorToDb(tmp_ratio)
                E_tmp_att = gainFactorToDb(E_tmp_ratio)
                #print("%0.2f %0.2f" % (tmpAtt, tmpEAtt), end=" ")
                #print("%0.2f" % (abs(tmpAtt - tmpEAtt)), end=" ")
                dev += abs(tmp_att - E_tmp_att)

    result[dev] = (R_in, attenuatorValues, E_attenuatorValues)
print(" Simulation done.")

if DEBUG:
    print("result dict:", result)
best = result[sorted(result.keys())[0]]
R_in, attenuatorValues, E_attenuatorValues = best[0], best[1], best[2]
print()
print("Found result:")
print("  Rin: %8.2f" % best[0])

TOLERANCE_FACTOR_LOWER = 1 - TOLERANCE
TOLERANCE_FACTOR_UPPER = 1 + TOLERANCE
deviation_sum = 0
for idx in range(0, len(DB_STEPS)):
    print("  R%02d: %8.2f -> %8.1f" % (idx * 2 + 1, attenuatorValues[idx][1], E_attenuatorValues[idx][1]))
    print("  R%02d: %8.2f -> %8.1f" % (idx * 2 + 2, attenuatorValues[idx][0], E_attenuatorValues[idx][0]))
    R_par, R_ser = E_attenuatorValues[idx][0], E_attenuatorValues[idx][1]
    if OPTIMIZE_LAST_TWO_STEPS and (idx + 3) > len(DB_STEPS):
        # choose series resistor one size smaller
        R_ser1 = E_RESISTOR_SERIES[E_RESISTOR_SERIES.index(E_attenuatorValues[idx][1]) + 1]
        target_attenuation = DB_STEPS[idx]
        current_deviation = 1000
        for E_R_try in reversed(E_RESISTOR_SERIES):
            E_R_ser_resulting = parallelResistors(R_ser1, E_R_try)
            tmp_ratio = parallelResistors(R_par, R_in) / (E_R_ser_resulting + parallelResistors(R_par, R_in))
            tmp_attenuation = gainFactorToDb(tmp_ratio)
            tmp_deviation = abs(target_attenuation - tmp_attenuation)
            if tmp_deviation < current_deviation:
                R_ser2 = E_R_try
                current_deviation = tmp_deviation
        R_ser = parallelResistors(R_ser1, R_ser2)
        print("  !!! R%02d consists of %0.1f and %0.1f in parallel." % (idx * 2 + 1, R_ser1, R_ser2))
    ratio = parallelResistors(R_par, R_in) / (R_ser + parallelResistors(R_par, R_in))
    ratio_lower = parallelResistors(R_par * TOLERANCE_FACTOR_UPPER, R_in) / \
                  (R_ser * TOLERANCE_FACTOR_LOWER + parallelResistors(R_par * TOLERANCE_FACTOR_UPPER, R_in))
    ratio_upper = parallelResistors(R_par * TOLERANCE_FACTOR_LOWER, R_in) / \
                  (R_ser * TOLERANCE_FACTOR_UPPER + parallelResistors(R_par * TOLERANCE_FACTOR_LOWER, R_in))
    deviation_range = gainFactorToDb(ratio_lower) - gainFactorToDb(ratio_upper)
    print("  Actual attenuation: %0.3fdB (%0.3fdB/%0.3fdB, deviation: %0.3fdB)" % (
        gainFactorToDb(ratio), gainFactorToDb(ratio_lower), gainFactorToDb(ratio_upper), deviation_range))
    deviation_sum += deviation_range
print("Absolute worst case summed deviation between two channels: %0.3fdB" % deviation_sum)
